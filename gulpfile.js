var gulp = require('gulp'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    watch = require('gulp-watch'),
    autoprefix = require('gulp-autoprefixer'),
    gulpMerge = require('gulp-merge'),
    concat = require('gulp-concat');

var paths = {
    source: "src",
    build: "dist",
    html: "src/*.html",
    css: "src/*.scss",
    components: "src/Components/*.scss"
};

gulp.task('build:html', () =>
    gulp.src(paths.html)
    .pipe(gulp.dest(paths.build))
);

gulp.task('build:css', () =>
    gulp.src(paths.css)
    .pipe(sass())
    .pipe(autoprefix({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(csso())
    .pipe(gulp.dest(paths.build))
);

gulp.task('build:lib', () => {
    return gulpMerge(
            gulp.src('src/theme.scss'),
            gulp.src('src/MerUI.scss')
            .pipe(sass())
            .pipe(autoprefix({
                browsers: ['last 2 versions'],
                cascade: false
            })))
        .pipe(concat('MerUI.scss'))
        .pipe(gulp.dest('lib'));
});

gulp.task('build:components', () =>
    gulp.src(paths.components)
    .pipe(sass())
    .pipe(autoprefix({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(csso())
    .pipe(gulp.dest(paths.build + '/components'))
);

gulp.task("build", [
    "build:lib",
    "build:html",
    "build:css",
    "build:components"
]);

gulp.task('watch', function() {
    gulp.watch([paths.html], ['build:html']);
    gulp.watch([paths.css], ['build:css']);
    gulp.watch([paths.components], ['build:components']);
});